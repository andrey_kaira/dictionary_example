import 'package:flutter/material.dart';
import 'package:flutter_dictionary_example/providers/language_provider.dart';
import 'package:provider/provider.dart';

import 'application/application.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      builder: (BuildContext ctx) {
        return LanguageProvider();
      },
      child: Application(),
    ),
  );
}

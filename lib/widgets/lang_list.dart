import 'package:flutter/material.dart';
import 'package:flutter_dictionary_example/dictionary/flutter_delegate.dart';
import 'package:flutter_dictionary_example/dictionary/models/supported_locales.dart';
import 'package:flutter_dictionary_example/providers/language_provider.dart';
import 'package:flutter_dictionary_example/res/shadow.dart';
import 'package:provider/provider.dart';

class LangList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250.0,
      margin: EdgeInsets.all(10),
      child: Consumer<LanguageProvider>(
        builder: (BuildContext context, LanguageProvider provider, Widget child) => Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            for (Locale loc in SupportedLocales.instance.getSupportedLocales)
              InkWell(
                onTap: () => provider.setNewLane(loc),
                child: AnimatedContainer(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(width: 5.0, color: Colors.black.withOpacity(0.5)),
                    boxShadow: FlutterDictionaryDelegate.getCurrentLocale.toString() == loc.toString()
                        ? Shadows.TEXT_SHADOW_SHOP
                        : null,
                    color: FlutterDictionaryDelegate.getCurrentLocale.toString() == loc.toString()
                        ? Colors.orange
                        : Colors.purple,
                  ),
                  padding: EdgeInsets.all(15),
                  duration: Duration(milliseconds: 400),
                  child: Text(
                    loc.toString().toUpperCase(),
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_dictionary_example/dictionary/data/en.dart';
import 'package:flutter_dictionary_example/dictionary/flutter_dictionary.dart';

import 'lang_list.dart';

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          AppBar(
            automaticallyImplyLeading: false,
              title: Text(FlutterDictionary.instance.language?.generalLanguage?.appTitle??en.generalLanguage.appTitle),
          ),
          InkWell(
            child: LangList(),
          ),
        ],
      ),
    );
  }
}

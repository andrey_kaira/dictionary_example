import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dictionary_example/dictionary/data/en.dart';
import 'package:flutter_dictionary_example/dictionary/dictionary_classes/home_page_language.dart';
import 'package:flutter_dictionary_example/dictionary/flutter_dictionary.dart';

import '../dictionary/flutter_dictionary.dart';

class GridViewItem extends StatelessWidget {
  final String url;
  final String subTitle;
  final int price;

  GridViewItem({
    this.subTitle,
    this.price,
    this.url,
  });

  @override
  Widget build(BuildContext context) {
    HomePageLanguage language = FlutterDictionary.instance.language?.homePageLanguage??en.homePageLanguage;
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(width: 4),
      ),
      child: GridTile(
        child: Image.network(
          url,
          fit: BoxFit.cover,
        ),
        footer: Container(
          height: 60,
          color: Colors.black.withOpacity(0.8),
          child: ListTile(
            title: AutoSizeText(
              language?.buyNow ??
                  en.homePageLanguage.buyNow,
              maxLines: 1,
            ),
            subtitle: AutoSizeText(
              subTitle,
              overflow: TextOverflow.ellipsis,
              minFontSize: 12,
              maxLines: 2,
              style: TextStyle(fontSize: 12),
            ),
            trailing: Container(
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.green,
              ),
              child: Text('$price.99 ${language.money}'),
            ),
          ),
        ),
      ),
    );
  }
}

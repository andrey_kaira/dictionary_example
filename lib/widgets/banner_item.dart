import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import '../dictionary/flutter_dictionary.dart';
import '../res/shadow.dart';

class BannerItem extends StatelessWidget {
  final String path;
  final String title;

  BannerItem({
    @required this.path,
    @required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            child: Image.network(
              path,
              fit: BoxFit.fitWidth,
            ),
          ),
          Container(
            alignment: FlutterDictionary.instance.isRTL
                ? Alignment.bottomRight
                : Alignment.bottomLeft,
            padding: EdgeInsets.all(16),
            child: AutoSizeText(
              title,
              maxLines: 2,
              textAlign: FlutterDictionary.instance.isRTL
                  ? TextAlign.right
                  : TextAlign.left,
              style: TextStyle(
                  fontSize: 33,
                  color: Colors.white,
                  shadows: Shadows.TEXT_SHADOW_TITLE,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }
}

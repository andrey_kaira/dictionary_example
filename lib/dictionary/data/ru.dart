import 'package:flutter_dictionary_example/dictionary/dictionary_classes/genaral_language.dart';
import 'package:flutter_dictionary_example/dictionary/dictionary_classes/home_page_language.dart';
import 'package:flutter_dictionary_example/dictionary/models/language.dart';

const Language ru = Language(
  generalLanguage: GeneralLanguage(
    appTitle: 'Тестовое приложение',
  ),
  homePageLanguage: HomePageLanguage(
    fresh: 'Свежое!',
    titleGame: 'Игровой',
    titleShop: 'Магазин',
    toContactUs: 'Связаться с нами:',
    buyNow: 'Купить',
    contact: 'Администратор: +38 (068) 42-93-776',
    banner1: 'Огромный выбор игр!',
    banner2: 'Лучшая игра года!',
    banner3: 'Легкое зарабатывание денег!',
    description1: 'Игра года',
    description2: 'Старые легенды',
    description3: 'Революция в мире игр',
    description4: 'Станьте настоящим самураем',
    description5: 'Неизвестная новинка',
    description6: 'Стань королем!',
    money: '*',
  ),
);

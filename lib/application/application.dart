import 'package:flutter/material.dart';
import 'package:flutter_dictionary_example/dictionary/flutter_delegate.dart';
import 'package:flutter_dictionary_example/dictionary/models/supported_locales.dart';
import 'package:flutter_dictionary_example/providers/language_provider.dart';
import 'package:flutter_dictionary_example/ui/home_page.dart';
import 'package:provider/provider.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<LanguageProvider>(
      builder: (BuildContext context, LanguageProvider provider, Widget _) {
        return MaterialApp(
          theme: ThemeData.dark(),
          debugShowCheckedModeBanner: false,
          locale: Locale(FlutterDictionaryDelegate.getCurrentLocale),
          supportedLocales: SupportedLocales.instance.getSupportedLocales,
          localizationsDelegates: FlutterDictionaryDelegate.getLocalizationDelegates,
          home: HomePage(),
        );
      },
    );
  }
}

import 'package:flutter/material.dart';

class Shadows {
  static List<BoxShadow> TEXT_SHADOW_GAME = [
    BoxShadow(
      offset: Offset(-1.0, 2.0),
      blurRadius: 8.0,
      color: Colors.blueAccent.withOpacity(0.4),
    ),
    BoxShadow(
      offset: Offset(1.0, 2.0),
      blurRadius: 8.0,
      color: Colors.blueAccent.withOpacity(0.4),
    ),
  ];

  static List<BoxShadow> TEXT_SHADOW_TITLE = [
    BoxShadow(
      offset: Offset(-1.0, 2.0),
      blurRadius: 16.0,
      color: Colors.black.withOpacity(0.9),
    ),
    BoxShadow(
      offset: Offset(1.0, 2.0),
      blurRadius: 16.0,
      color: Colors.black.withOpacity(0.9),
    ),
  ];

  static List<BoxShadow> TEXT_SHADOW_SHOP = [
    BoxShadow(
      offset: Offset(-1.0, 2.0),
      blurRadius: 8.0,
      color: Colors.yellow.withOpacity(0.4),
    ),
    BoxShadow(
      offset: Offset(1.0, 2.0),
      blurRadius: 8.0,
      color: Colors.yellow.withOpacity(0.4),
    ),
  ];
}

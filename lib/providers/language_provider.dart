import 'package:flutter/cupertino.dart';
import 'package:flutter_dictionary_example/dictionary/flutter_dictionary.dart';

class LanguageProvider extends ChangeNotifier {
  void setNewLane(Locale locale) {
    FlutterDictionary.instance.setNewLanguage(locale.toString());
    notifyListeners();
  }
}
